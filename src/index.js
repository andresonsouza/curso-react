import React from 'react'
import ReactDOM from 'react-dom'
// import PrimeiroComponente from './componentes/PrimeiroComponente'
// import CompA, { CompB as B } from './componentes/DoisComponentes'
// import MultiElementos from './componentes/MultiElementos'

import FamiliaSilva from './componentes/FamiliaSilva'

const elemento = document.getElementById('root')
ReactDOM.render(
    <div>
        <FamiliaSilva />
        {/* <MultiElementos /> */}
        {/* <CompA valor="Olá! Eu sou o componente A" /> */}
        {/* <B valor="Cheguei mano!!!" /> */}
        {/* <PrimeiroComponente valor="Bom dia!" /> */}
    </div>
    , elemento) 