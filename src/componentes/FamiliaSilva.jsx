import React from 'react'
import Membro from './Membro'

export default props =>
    <div>
        <Membro nome="Naguiza" sobrenome="Silva" />
        <Membro nome="Josefa" sobrenome="Silva" />
        <Membro nome="Vicente" sobrenome="Silva" />
        <Membro nome="Nivea" sobrenome="Silva" />
    </div>